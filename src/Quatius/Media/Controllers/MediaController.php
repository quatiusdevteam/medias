<?php 

namespace Quatius\Media\Controllers;

use Illuminate\Http\Request;
use Quatius\Media\Repositories\MediaRepository;
use App\Http\Controllers\AdminWebController;
use File;
use Quatius\Media\Models\Media;

use Response;

class MediaController extends AdminWebController
{
	private $repo;
	public function __construct(MediaRepository $repo)
	{
		parent::__construct();
		$this->repo = $repo;
		$this->list_url = 'admin/medias';
	}
	
    public function index(Request $request){
        $search = $request->get('search', '');
        if ($request->has('search'))
        {
            $medias = Media::where('title', 'LIKE', '%'.$search.'%')->orderby('updated_at','desc')->paginate(14);
        }
        else
            $medias = Media::orderby('updated_at','desc')->paginate(14);
            
        if ($request->has('page') or $request->exists('search'))
            return view('Media::admin._list',compact('search', 'medias'));
        
        return $this->theme->of('Media::admin.index',compact('search', 'medias'))->render();
		
	}
	
    /* public function indexProductPage(Request $request){
    	$type_id = 0;
    	$medias = Media::whereType($type_id)->orderby('updated_at','desc')->paginate(14);
    	$medias->setPath(url('admin/medias/product/page'));
    	return view('Media::admin._list',compact('type_id', 'medias'));
    }
     */
    public function getMediaPath(Request $request){
		$path = $request->get("path", "");
        $medias = $this->repo->getMediaFromPath($path,$request->get("mime_types", ""));
        
        $view = view('Media::admin.partials.list',compact('medias','path'))->render();
        
        return response()->json(
            [
                'message'  => "",
                'view'  => $view,
                'path' => $path,
                'mime_types'=>$request->get("mime_types", "")
            ],
            200);
    }
    
    public function uploadMedia(Request $request){
        $view = "";
		$id = 0;
		$pathView = "";
		$path = $request->get('path', config('quatius.media.default_path', ""));

		if ($request->hasFile('file') && $path)
    	{
    		$file = $request->file('file');
			$force = $request->get('force',[]);
			$allow_stretch = $request->get('allow_stretch', config('quatius.media.allow_stretch', true)?1:0);
			
			// when new path
			$isPathExist = (!!\Quatius\Media\Models\Media::whereFilePath($path)->count());

			$media = null;
			if (count($force) == 2)
			    $media = $this->repo->upload($file, $path, $force['width'], $force['height'], true, $allow_stretch, $request->get('sizes',[]));
			else
			    $media = $this->repo->upload($file, $path, null, null, true, $allow_stretch, $request->get('sizes',[]));
			
			if($request->has('media_link') && $request->has('media_link_id'))
			{
			    $media->links($request->get('media_link'))->sync([$request->get('media_link_id')], false);
			}
			$id = $media->id;
			$view = view('Media::admin.partials.tile', ['media'=>$media])->render();
			
			if(!$isPathExist){
				$pathView = view('Media::admin.partials.folder-list')->render();
			}
    	}
    	
    	return response()->json(
    	    [
    	        'message'  => "",
    	        'view'  => $view,
				'id'  => $id,
				'path_view'=> $pathView
    	    ],
    	    200);
    }
    
    public function delete(Request $request, $id){
    	$this->repo->deleteFiles([$id]);  
    	return response()->json(
    	    [
    	    ],
    	    201);
	}
	public function test(Request $request){
		$source_url="http://cybercast-sydney.s3.amazonaws.com/tmp/hdms/sources/7e2b1d7b37e2d71dfc27b6e8f02af8ad.png";
		//$source_url="http://cybercast-sydney.s3.amazonaws.com/tmp/hdms/sources/a62c1575071814e0638a7f9ab73424ce.jpg";
		
		$path = "images/4px";
		//remote source setting.
		$datas = $this->repo->loadFileData($source_url, $path);
		$media = new Media($datas);
		//$media->save();
		
		
    	return response()->json(
    	    [
				"source_url"=>$source_url,
				"datas"=>$media,
    	    ],
    	    201);
    }  
   /*  
    public function availableMedias(Request $request, $format = 'html'){
    	$term = $request->input('term');
    	$product_id = $request->input('product_id');
    	$q = Media::whereStatus(2)->resized(120,120);
    	if(!empty($product_id)){
    		$q->whereNotExists(function($query) use(&$product_id){
    			$query->select(DB::raw(1))
    			->from('media_product AS pm')
    			->whereRaw('medias.id=pm.media_id')
    			->where('pm.product_id',$product_id);
    		});
    	}
    	$medias = $q->where(function($query) use (&$term){
    		$query->where('title','LIKE','%'.trim($term).'%')
    			->orWhere('description','LIKE','%'.trim($term).'%');
    	})->orderBy('medias.id')->get();
    	$result = array();
    	foreach($medias as $media){
    		$result[] = array('key'=>$media->id,'value'=>$media->title,'link'=>'/'.$media->file_path.'/'.$media->width.'x'.$media->height.'/'.$media->file_name);
    	}
    	return response()->json($result);
    } */
}
