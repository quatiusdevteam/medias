<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
//Make only
$factory->define(Quatius\Media\Models\Media::class, function(Faker\Generator $faker) {
    $name = $faker->word;
    $imagePath = $faker->image(sys_get_temp_dir(),400,300, null, true, true);
    $file = new \Illuminate\Http\UploadedFile($imagePath, "$name.jpg", 'image/jpeg', filesize($imagePath));
    $media = Media::upload($file, 'images');
    if (!$media)
        return [];
    
    return $media->toArray();
});