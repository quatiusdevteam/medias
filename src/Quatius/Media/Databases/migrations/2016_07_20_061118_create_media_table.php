<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',200);
            $table->string('description',1000)->nullable();
            $table->string('file_name');
            $table->string('file_path',1000);
            $table->integer('file_size')->default(0);
            $table->string('mime_type',100);
            $table->integer('width')->nullable()->default(0); // in pixels
            $table->integer('height')->nullable()->default(0); // in pixels
            $table->string('downloadable',100);
            $table->tinyInteger('allow_stretch')->default(1);
            $table->tinyInteger('use_cloud')->default(0);
            $table->tinyInteger('status');
            $table->timestamps();
        });
        Schema::create('media_resizes',function(Blueprint $table){
        	$table->increments('id');
        	$table->integer('media_id')->unsigned();
        	$table->integer('width'); // in pixels
        	$table->integer('height'); // in pixels
        	$table->tinyInteger('use_cloud')->default(0);
        	$table->foreign('media_id')
            	->references('id')
            	->on('medias')
            	->onDelete('cascade');
        });
		Schema::create('media_links', function (Blueprint $table) {
    		$table->integer('media_link_id')->unsigned();
    		$table->string('media_link_type');
    		$table->integer('media_id')->unsigned();
    		$table->integer('ordering')->unsigned()->default(0);
    		$table->string('grouping',50)->nullable()->default("image");
    		$table->text('params')->nullable();
    		$table->foreign('media_id')
        		->references('id')
        		->on('medias')
        		->onDelete('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_resizes');
        Schema::dropIfExists('media_links');
        Schema::dropIfExists('medias');
    }
}
