<?php 

namespace Quatius\Media\Models;

use Illuminate\Database\Eloquent\Model;
use App;

/**
 * @author seyla.keth
 *
 */
class Media extends Model {
    
    protected $fillable = [
    		'title',
    		'description',
    		'file_path',
    		'file_name',
    		'file_size',
    		'source_url',
            'mime_type',
            'width',
            'height',
    		'downloadable',
            'allow_stretch',
            'use_cloud',
    		'status',
    ];
    
    protected $table = 'medias';
    
    public function resizes()
    {
        return $this->hasMany(MediaResize::class);
    }
    
    public function links($relationClass)
    {
        return $this->morphedByMany($relationClass, 'media_link')->withPivot('ordering', 'params');
    }
    
	public function getPathAttribute()
    {
        return ($this->file_path==""?"":$this->file_path."/").$this->file_name;
    }
	
    public function scopeSearch($query, $search){
    	$s = trim($search);
    	if(!empty($s)){
	    	$query->where(function($q) use ($s){
	    		$q->orWhere('title','LIKE','%'.$s.'%')
	    		  ->orWhere('slug','LIKE','%'.$s.'%')
	    		  ->orWhere('file_name','LIKE','%'.$s.'%');
	    	});
    	}
    	return $query;
    }
    
    public function scopeStatus($query , $status){
    	if(!empty($status)){
    		$query->whereStatus($status);
    	}
    	return $query;
    }
    
    public function scopeResized($query, $width=0, $height=0){
    	$query->leftJoin('media_resizes', 'medias.id','=','media_resizes.media_id')
    		->where('media_resizes.width',$width)
    		->where('media_resizes.height',$height)
    		->selectRaw('medias.*, media_resizes.*');
    }

    public function getThumbUrl($width=0, $height=0, $srcFile= null)
    {
        if ($width==0)
            return $this->getThumbnailUrl();
        try{
            return media_url($this->resizeTo($width, $height, false, $srcFile, $this->allow_stretch));
        }
        catch (\Exception $e){
            if (config("quatius.media.debug", false)) app('log')->debug("Media- getThumbUrl(".$e->getLine()."): ".$e->getMessage());
            
            return "";
        }
    }

    public function getSize($label='thumbnail', $srcFile=null)
    {
        if (!config('quatius.media.sizes.'.$label)) throw new \Exception("Unknown Image size: $label");

        try{
        return $this->resizeTo(
            config("quatius.media.sizes.$label.width", 120), 
            config("quatius.media.sizes.$label.height", 90), 
            false, $srcFile, 
            config("quatius.media.sizes.$label.allow_stretch", $this->allow_stretch));
        }catch (\Exception $e){
            if (config("quatius.media.debug", false)) app('log')->debug("Media- Unable to read Image(".$e->getLine()."): ".$e->getMessage());
            
            //throw new \Exception("Unable to read Image: " . $e->getMessage());
        }
    }
    
    public function resizeTo($width=null, $height=null, $recreate=false, $srcFile=null, $allow_stretch = null)
	{
	    if (!$width && !$height || !$this->isImage()){
	        return null;
	    }
	    
	    // resize auto scale
	    if (!$width || !$height){
	        if (!$height){
	            $height = round(($width / $this->width) * $this->height);
	        }elseif (!$width){
	            $width = round(($height / $this->height) * $this->width);
	        }
	    }
	    
	    $allow_stretch = $allow_stretch===null?$this->allow_stretch:$allow_stretch;
	    $repoDB = App::make('Quatius\Media\Repositories\MediaRepository');
	    $resize = $repoDB->getResize($this, $width,$height);
	    
	    if ($resize && !$recreate)
	    {
	        $resize->setRelation('media', $this);
	        if($resize->use_cloud){ // already on cloud server
	            return $resize;
	        }
	        
	        $putSizeOnCloud = ($this->use_cloud && config('quatius.media.cloud_writable',false))?1:0;
	        if (!$putSizeOnCloud) {
	            if ($repoDB->hasStorage($resize->path, 0)) // check local storage
	               return $resize;
	        }
	        
	        // if come here we will recreate it. 
	        $recreate = true;
	    }
	    
	    $resize = $repoDB->resize($this, $width, $height, $recreate, $srcFile, $allow_stretch);
		            
		if($resize)
		{
		    $this->load('resizes'); // update relations.
            $resize->setRelation('media', $this);
		    return $resize;
		}
		return null;
	}
	
    public function getThumbnailUrl($size = 'thumbnail'){
        return media_url($this->getSize($size));
    }
    
    public function scopePublished($query){
        return $query->whereStatus(2);
    }
    
    public function isImage(){
        return isSupportedImage($this->file_name);
    }
    
    public function getMediaUrl(){
        return media_url($this);
    }
    
    public function getImageUrl($width=null, $height=null, $allow_stretch = true){
        
        if (!$this->isImage() || //not image
            $this->mime_type == 'image/gif' || //not image
            (!$width && !$height) || // both are null
            ($width == $this->width && $height == $this->height )) // same dimension as requested
        {
            return $this->getMediaUrl();
        }
        
        //when either width or height is null then calculate the ratio size.
        if (!$height){
            $scale = $width / $this->width;
            $height = round($this->height * $scale);
        }
        elseif (!$width){
            $scale = $height / $this->height;
            $width = round($this->width * $scale);
        }
        
        try{
            $resized = $this->resizeTo($width, $height, false, null, $allow_stretch);
            
            //return a resized url
            return media_url($resized);
        }
        catch (\Exception $e){
            if (config("quatius.media.debug", false)) app('log')->debug("Media- getImageUrl(".$e->getLine()."): ".$e->getMessage());
            return "";
        }
    }

    public function isRemoteUrl(){
        return (!$this->file_size) && (!!$this->source_url);
    }
}
