<?php 

namespace Quatius\Media\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author seyla.keth
 *
 */
class MediaResize extends Model {

    protected $fillable = [
    		'width',
    		'height',
            'use_cloud'
    ];
    
    public $timestamps = false;
    
    public function media(){
        return $this->belongsTo(Media::class);
	}
	
	public function isRemoteUrl(){
        return false;
	}
	
    public function ScopeDimension($query, $width, $height){
        return $query->where("width",$width)->where("height",$height);
    }
	
	public static function generateSizePath($media, $width, $height)
	{
	    $path = config('quatius.media.resize_name_prefix', "sizes/").$width.'x'.$height.'/';
	    if ($media instanceof MediaResize){
	        
	        return $path.$media->media->path;
	    }
	    elseif ($media instanceof Media){
	        return $path.$media->path;
	    }
	    return "";
		//return $path.$width.'x'.$height.'_'.$filename;
	}
	
	public function getPathAttribute()
    {
        return self::generateSizePath($this, $this->width, $this->height);//($this->media->file_path==""?"":$this->media->file_path."/").$this->rename;
    }
}
