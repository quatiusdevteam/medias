<?php

namespace Quatius\Media\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Factory;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
        $this->publishes([__DIR__.'/../Publishes/Public'=>public_path()],'public');
    }
	
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Quatius\Media\Repositories\MediaRepository','Quatius\Media\Repositories\DBMediaRepository');
        
        App::bind('MediaHandler', function()
        {
            return app('Quatius\Media\Repositories\MediaRepository');
        });
        
        AliasLoader::getInstance()->alias('Media', 'Quatius\Media\Facades\MediaFacade');
        $this->app->make(Factory::class)->load(__DIR__.'/../Databases/factories');
        
    }
}
