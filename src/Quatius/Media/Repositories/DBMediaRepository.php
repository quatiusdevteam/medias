<?php

namespace Quatius\Media\Repositories;

use Illuminate\Http\UploadedFile;
use File;
use DB;
use Exception;
use Quatius\Media\Models\Media;
use Image;
use Storage;
use Quatius\Media\Models\MediaResize;

use Illuminate\Database\Eloquent\Model;
use Quatius\Framework\Repositories\QuatiusRepository;

class DBMediaRepository extends QuatiusRepository implements MediaRepository{
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Quatius\Media\Models\Media::class;
    }
    
    private function _getMediaFromPath($path="", $mime_types=""){
        $mediaQuery = Media::published();
        
        if ($path != ""){
            $mediaQuery->whereFilePath($path);
        }
        if ($mime_types != ""){
            $mediaQuery->whereIn("mime_type",explode(',',$mime_types));
        }
        return $mediaQuery->get();
    }
    
    public function getMediaFromPath($path="", $mime_types=""){
        if (!$this->allowedCache('getMediaFromPath') || $this->isSkippedCache()) {
            return $this->_getMediaFromPath($path, $mime_types);
        }
        
        $key = $this->getCacheKey('getMediaFromPath', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($path, $mime_types) {
            return $this->_getMediaFromPath($path, $mime_types);
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    public function loadLinks($model){
        if ($model instanceof Model){
            $model->setRelation('medias', app('MediaHandler')->getLinkFrom($model));
        }else if ($model instanceof \Illuminate\Support\Collection && $model->first()){
            
            $medias = $this->getLinkFrom(get_class($model->first()),$model->pluck('id')->toArray());

            $model->map(function($modelEa) use($medias){
                $modelEa->setRelation('medias', $medias->where('pivot.media_link_id', $modelEa->id));
            });
        }
    }

    public function deleteLinkFrom($relationClass, $id=null, $byMediaIds=null, $grouping=null){
        $classPath = "";
        $classId = $id;
        if (is_string($relationClass)){
            $classPath = $relationClass;
        }
        elseif ($relationClass instanceof Model)
        {
            $classPath = get_class($relationClass);
            $classId = $relationClass->id;
        }
        if (!$classId || $classPath == ""){
            return;
        }

        $mediaQuery = DB::table("media_links")->whereMediaLinkType($classPath)->whereMediaLinkId($classId);
        if ($byMediaIds!=null){
            if (!is_array($byMediaIds)){
                $byMediaIds = [$byMediaIds];
            }
            if (count($byMediaIds) == 0)
                return;
            $mediaQuery->whereIn('media_id',$byMediaIds);
        }

        if ($grouping !== null)
            $mediaQuery->whereGrouping($grouping);

        $this->clearCache();
        return $mediaQuery->delete();
    }
    
    private $applyCloud = null;
    
    public function updateLink($media_ids, $relationClass, $id = null, $detaching=true, $grouping = null){
        
        $classPath = "";
        $classId = $id;
        
        if (is_string($relationClass)){
            $classPath = $relationClass;
        }
        elseif ($relationClass instanceof Model)
        {
            $classPath = get_class($relationClass);
            $classId = $relationClass->id;
        }
        
        if (!$classId){
            return;
        }
        
        $mediaLinks = DB::table("media_links");
        
        if ($detaching){
            $this->deleteLinkFrom($classPath, $classId, null, $grouping);
        }

        $ordering = 0;
        foreach ($media_ids as $data){
            $data['media_link_id'] = $classId;
            $data['media_link_type'] = $classPath;
            $data['grouping'] = isset($data['grouping'])?$data['grouping']:($grouping === null?'image':$grouping);
            if ($detaching){
                $data['ordering'] = ++$ordering;
            }
            $mediaLinks->insert($data);
        }
        $this->clearCache();
    }
    
    public function updateLinkOrdering($media_ids, $relationClass, $id = null){
        
        $classPath = "";
        $classId = $id;
        
        if (is_string($relationClass)){
            $classPath = $relationClass;
        }
        elseif ($relationClass instanceof Model)
        {
            $classPath = get_class($relationClass);
            $classId = $relationClass->id;
        }
        
        if (!$classId){
            return;
        }
        
        $mediaLinks = DB::table("media_links");
        
        $ordering = 10;
        for ($m=0; $m < count($media_ids); $m++){
            $media_ids[$m]['ordering'] = $m + 1;
            DB::table("media_links")
            ->where('media_link_id', $classId)
            ->where('media_link_type', $classPath)
            ->where('media_id', $media_ids[$m]['media_id'])
            ->update(['ordering'=> $media_ids[$m]['ordering']]);
        }
        $this->clearCache();
        return $media_ids;
    }
    
    public function getLinkFrom($relationClass, $id=null, $withResizeRelation = true, $grouping=null, $firstOnly=false)
    {
        if (!$this->allowedCache('getLinkFrom') || $this->isSkippedCache()) {
            return $this->_getLinkFrom($relationClass, $id, $withResizeRelation, $grouping, $firstOnly);
        }
        
        $key = $this->getCacheKey('getLinkFrom', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($relationClass, $id, $withResizeRelation, $grouping, $firstOnly) {
            return $this->_getLinkFrom($relationClass, $id, $withResizeRelation, $grouping, $firstOnly);
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    private function _getLinkFrom($relationClass, $id=null, $withResizeRelation = true, $grouping=null, $firstOnly=false)
    {
        if ($firstOnly){
            $mediaDb = $this->_getFirstLinkFrom($grouping?:"image");
        }else{
            $mediaDb = DB::table("media_links");
        }
        
        if (is_string($relationClass)){
            $mediaDb->whereMediaLinkType($relationClass);
        }
        elseif (is_array($relationClass)){
            $mediaDb->whereIn('media_link_type', $relationClass);
        }
        elseif ($relationClass instanceof Model){
            
            $mediaDb->whereMediaLinkType(get_class($relationClass));
            if (!$relationClass->exists) return collect([]);
            
            $id = $relationClass->id;
        }
        
        if(is_scalar($id)){
            $mediaDb->whereMediaLinkId($id);
        }elseif (is_array($id)){
            $mediaDb->whereIn('media_link_id', $id);
        }
        
        $mediaLinks = $mediaDb->get();
        
        $mediaIds = [];
        
        foreach ($mediaLinks as $link){
            $mediaIds[] = $link->media_id;
        }
        $newmedias = [];
        
        $mediaQuery = Media::whereIn('id', $mediaIds);
        if ($withResizeRelation){
            $mediaQuery->with("resizes");
        }
        
        $medias = $mediaQuery->get();
        foreach($mediaLinks as $link){
            $tmpmedias = $medias->where('id',intval($link->media_id))->first();
            if(isset($tmpmedias->pivot)){
                $tmpmedias = clone $tmpmedias;
            }
            $tmpmedias->pivot = $link;
            $newmedias[] = $tmpmedias;
        }
        
        return collect($newmedias)->sortBy('pivot.ordering');
    }
    
    private function _getThumbLinkFrom($relationClass, $ids, $width, $height, $group='image', $excluseGifResize = true)
    {
        $thumbUrls = [];
        $thumbList = $this->_getLinkFrom($relationClass, $ids, true, $group, true);
        foreach ($thumbList as $thumb){
            if ($excluseGifResize && $thumb->mime_type == 'image/gif'){
                $thumbUrls[$thumb->pivot->media_link_id] = media_url($thumb);
            }
            else{
                $thumbUrls[$thumb->pivot->media_link_id] = media_url($thumb->resizeTo($width, $height, false, null, false));
            }
        }
        
        return $thumbUrls;
    }
    public function getThumbLinkFrom($relationClass, $ids, $width, $height, $group='image', $excluseGifResize = true)
    {
        if (!$this->allowedCache('getThumbLinkFrom') || $this->isSkippedCache()) {
            return $this->_getThumbLinkFrom($relationClass, $ids, $width, $height, $group, $excluseGifResize);
        }
        
        $key = $this->getCacheKey('getThumbLinkFrom', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($relationClass, $ids, $width, $height, $group, $excluseGifResize) {
            return $this->_getThumbLinkFrom($relationClass, $ids, $width, $height, $group, $excluseGifResize);
        });
            
            $this->resetModel();
            $this->resetScope();
            return $value;
    }
    
    public function _getFirstLinkFrom($group='image')
    {
        $mediaDb = DB::table("media_links");
        
        $mediaDb->leftJoin(DB::raw("(SELECT media_link_id as medlinkid, min(ordering)as minord FROM `media_links` WHERE `grouping`='$group' GROUP BY media_link_id) as uniqmedia"), function($join){
            $join->on('media_link_id','medlinkid');
            $join->on('ordering','minord');
        });
        $mediaDb->whereNotNull('minord');
        
        return $mediaDb;  
    }
    
    function loadFileData($file, $base_path='', $allow_stretch=1, $randomFileName=false)
    {
        $base_path = str_replace("\\","/", $base_path);
        $datas = [
            'file_path'=>$base_path,
            'downloadable'=>1,
            'status'=>2,
            'allow_stretch'=>$allow_stretch,
            'use_cloud'=>$this->useCloud()?1:0,
            'width'=>0,
            'height'=>0
        ];
        
        if (is_string($file))
        {
            if (File::isFile($file)){
                $filename = self::sanitizeFileName(basename($file));
                
                $datas['title'] = File::name($filename);
                $datas['file_name'] = $filename;
                $datas['file_size'] = File::size($file);
                $datas['mime_type'] = File::mimeType($file);
            }elseif(filter_var($file, FILTER_VALIDATE_URL) !== false){
                $filename = self::sanitizeFileName(basename($file));
                $datas['title'] = File::name($filename);
                $datas['source_url'] = $file;
                $datas['file_name'] = $filename;
                $datas['mime_type'] = getMimeType(pathinfo($filename, PATHINFO_EXTENSION));
            }
        }
        elseif ($file instanceof UploadedFile)
        {
            $filename = self::sanitizeFileName($file->getClientOriginalName());
            $datas['title'] = File::name($filename);
            $datas['file_name'] = $filename;
            $datas['file_size'] = $file->getClientSize();
            $datas['mime_type'] = File::mimeType($file);
        }
        else{
            return null;
        }
        if ($randomFileName){
            $datas['file_name'] = md5($datas['file_name'].time()).substr($datas['file_name'],strrpos($datas['file_name'],"."));
        }

        $datas['source'] = $file;
        return $datas;
    }
    
    function upload($fileData, $base_path='', $forceWidth=null, $forceHeight=null, $update = true, $allow_stretch=null, $reqSizes=[]){
		if (config('quatius.media.memory_limit','') != ''){
			ini_set('memory_limit',config('quatius.media.memory_limit',''));
		}
        
        $datas = null;
        if (!is_array($fileData)){
            $allow_stretch = $allow_stretch===null?(config('quatius.media.allow_stretch', true)?1:0):$allow_stretch;
            $datas = $this->loadFileData($fileData, $base_path, $allow_stretch, false);
        }
        else{
            $datas = $fileData;
        }
        
        if (!$datas)
            return null;

        $file = $datas['source'];
        unset($datas['source']);

        $success = false;
        
        $media = Media::whereFileName($datas['file_name'])->whereFilePath($datas['file_path'])->first();
        
        if (!$media){
            $media = new Media($datas);
        }
        
        if ($media->isImage())
        {
            if ($media->exists){
                $this->deleteFiles([$media->id],true, true); // delete resize reference
            }
            
            if (($forceWidth * $forceHeight)!==0)
            {
                if ($media->allow_stretch == 0){
                    $img = $this->resizeImageToCanvas($file, $forceWidth, $forceHeight);
                }
                else{
                    $img = $this->resizeImage($file, $forceWidth, $forceHeight);
                }
                $binary = $img->stream()->__toString();
                
                $media->file_size = strlen($binary);
                $media->width = $forceWidth;
                $media->height = $forceHeight;
                
                $success = $this->putStorage($media, $binary);
            }
            elseif(isset($datas['source_url'])){
                $img = Image::make($datas['source_url']);
                $img->orientate();
                $media->width = $img->width();
                $media->height = $img->height();
                $binary = $img->stream()->__toString();
                $media->file_size = strlen($binary);

                $success = $this->putStorage($media, $binary);
            }
            else{
                $img = Image::make($file);
				$img->orientate();
                $media->width = $img->width();
                $media->height = $img->height();
                
                $success = $this->putStorage($media, File::get($file));
            }
        }
        else{
            $success = $this->putStorage($media, File::get($file));
        }
        $this->clearCache();
        if ($success)
        {
            $media->save();

            //Auto resize
            $sizes = array_merge(config('quatius.media.sizes', []), $reqSizes);
			if ($media->isImage()){
                foreach($sizes as $label=>$size)
                {
                    if(!isset($size['width']) || !isset($size['height'])) {
                        continue;
                    }
                        
                    if (!isset($size['create_on_upload']) || $size['create_on_upload'] == false){
                        continue;
                    }
                    
                    $streching = isset($size['allow_stretch'])?$size['allow_stretch']:$media->allow_stretch;
                    $this->resize($media, $size['width'],$size['height'], true, $file, $streching);
                }
            }
            return $media;
        }
        return null;
    }
    
    function useCloud($setCloud = null){
        if ($setCloud !== null){
            return $setCloud == 1;
        }
        else{
            return config('quatius.media.cloud_storage', false);
        }
    }
    
    public static function sanitizeFileName($dangerousFilename, $platform = 'Unix')
    {
        $dangerousFilename = trim($dangerousFilename);
        
        $ext = substr($dangerousFilename,strrpos($dangerousFilename,'.'));
        $name = substr($dangerousFilename,0,strrpos($dangerousFilename,'.'));
        
        $dangerousFilename = $name.strtolower($ext);
        
        if (in_array(strtolower($platform), array('unix', 'linux'))) {
            // our list of "dangerous characters", add/remove
            // characters if necessary
            $dangerousCharacters = array('"', "'", "&", "/", "\\", "?", "#");
        } else {
            // no OS matched? return the original filename then...
            return $dangerousFilename;
        }
        
        // every forbidden character is replace by an underscore
        return str_replace($dangerousCharacters, '_', $dangerousFilename);
    }
    
    function getResize($media, $width, $height){
        $resize = $media->resizes->where("width",intVal($width))->where("height",intVal($height))->first();
        if(!$resize){ // bug in some having different behavior string and int
            return $media->resizes->where("width","$width")->where("height","$height")->first();
        }
        
        return $resize;
    }
    
    function getOptions($media, $onCloud = null){
        $option = (object) array("path"=>"", "cloud"=>$this->useCloud($onCloud));
        
        if ($media instanceof Media || $media instanceof MediaResize){
            if ($onCloud === null){
                $option->cloud = $this->useCloud($media->use_cloud);
            }
            $option->path = $media->path;
        }
        elseif (is_string($media)){
            $option->path = $media;
        }
        $option->path = config('quatius.media.root_path', 'medias/').$option->path;
        return $option;
    }
    
    function putStorage($path, $file, $onCloud = null, $visible = 'public'){
		$status = false;
        $option = $this->getOptions($path, $onCloud);
        if ($option->cloud){
            abort_if(!config('quatius.media.cloud_writable',false),302, 'Media cloud set to not be writable in debug mode');
        }
        $storage = $this->storage($option->cloud);
        $status = $storage->put($option->path, $file);

        if ($status)
            $storage->setVisibility($option->path, $visible);

        return $status;
    }
    
    function getStorage($path, $onCloud = null){
        try{
            $option = $this->getOptions($path, $onCloud);
            if ($option->cloud){
                if (config('quatius.media.fetch_remote_url','')) {
                    return file_get_contents(config('quatius.media.fetch_remote_url','').str_replace('%2F','/', rawurlencode($option->path)));
                }
                return Storage::cloud()->get($option->path);
            }
            else{
                if (!file_exists($option->path))
                    $option->path = public_path($option->path);
                
                if (!file_exists($option->path)) return "";

                return file_get_contents($option->path);
            }
        }catch(\Exeception $e){
            if (config("quatius.media.debug", false)) app('log').debug("Media- getStorage(".$e->getLine()."): ".$e->getMessage());
            return "";
        }
    }
    
    function hasStorage($path, $onCloud = null){
        $option = $this->getOptions($path, $onCloud);

        if ($option->cloud && config('quatius.media.fetch_remote_url','')){
            $url = config('quatius.media.fetch_remote_url','').str_replace('%2F','/', rawurlencode($option->path));
            $headers = @get_headers($url);
            return strpos($headers[0],'404') === false;
        }
        return $this->storage($option->cloud)->has($option->path);
    }
    
    function storage($onCloud)
    {
        if ($this->useCloud($onCloud)){
            return Storage::cloud();
        }
        else{
            $localFile = new \League\Flysystem\Filesystem(new \League\Flysystem\Adapter\Local(public_path()));
            return $localFile;
        }
    }
    
    function deleteStorage($path, $onCloud = null){
        $option = $this->getOptions($path, $onCloud);
        
        abort_if($option->cloud && !config('quatius.media.cloud_writable',false),
            302, 'Media cloud set to not be writable in debug mode');

        return $this->storage($option->cloud)->delete($option->path);
    }
    
    function url($src, $width=null, $height=null, $allow_stretch=null){
        
        if (is_string($src)){
            $media = $this->findWhere(['file_name'=>basename($src), 'file_path'=>dirname($src)])->first();
        }
        else {
            $media = $src;
        }
        
        if (!$media) {
            return config('quatius.media.not_found_url', "");
        }
        
        if ($media->width == 0 && $media->height == 0 && $media->isRemoteUrl()){
            getRemoteDimension($media);
        }
        
        if ($width || $height){
            $media = $media->resizeTo($width, $height, false, null, $allow_stretch);
        }
        
        if (!$media) {
            return config('quatius.media.not_found_url', "");
        }
        
        if ($media->isRemoteUrl()){
            if ($media->width == 0 && $media->height == 0){
                getRemoteDimension($media);
            }
            return $media->source_url;
        }

        if (config('quatius.media.check_url_exist',false) && !$this->hasStorage($media)){
            return config('quatius.media.not_found_url', "");
        }

        $option = $this->getOptions($media);

        if ($option->cloud){
            if (config('quatius.media.fetch_remote_url','')) {
                return config('quatius.media.fetch_remote_url','').$option->path;
            }
            return Storage::cloud()->url($option->path);
        }
        else{
            return url($option->path);
        }
    }
    
    static function resizeImageToCanvas($src, $width, $height)
    {
        if($width * $height===0) return null;
		
        if (config('quatius.media.memory_limit','') != ''){
			ini_set('memory_limit',config('quatius.media.memory_limit',''));
		}
		
        $img = Image::make($src); //incase src and target is the same.
        $img->orientate();
        if ($img->width() == $img->height()) // square
        {
            $minSize = $width > $height? $height: $width;
            
            $img->resize($minSize, $minSize, function ($constraint) {
                $constraint->upsize();
            });

            $img->resizeCanvas($width, $height);
            return $img;
        }
        
        $resizeWidth = $width;
        $resizeHeight = $height;
        
        if ($img->width() > $img->height())
        {
            $scale = $width / $img->width();
            if ($scale * $img->height() > $height) {// height is cut off
                $resizeWidth = null; // constrain aspect ratio (auto width)
            }
            else {
                $resizeHeight = null;// constrain aspect ratio (auto height)
            }
        }
        else
        {
            $scale = $height / $img->height();
            if ($scale * $img->width() > $width) {// width is cut off
                $resizeHeight = null; // constrain aspect ratio (auto height)
            }
            else{
                $resizeWidth = null; // constrain aspect ratio (auto width)
            }
        }
        
        $img->resize($resizeWidth, $resizeHeight, function ($constraint) {
			$constraint->upsize();
            $constraint->aspectRatio();
		});
        
        $img->resizeCanvas($width, $height);


        return $img;
    }
    
    
    static function resizeImage($src, $width, $height)
    {
        if($width * $height===0) return null;
        
		if (config('quatius.media.memory_limit','') != ''){
			ini_set('memory_limit',config('quatius.media.memory_limit',''));
		}
		
        $img = Image::make($src);
        $img->orientate()
		->resize($width, $height, function ($constraint) {
			$constraint->upsize();
		});
        return $img;
    }
    
    function resizeToCanvas(Media $media, $width=0, $height=0, $recreate=false, $srcFile=null){
        return $this->resize($media, $width, $height, $recreate, $srcFile, false);
    }
    
    function resize($media, $width=0, $height=0, $recreate=false, $srcFile=null, $allowStretch=true){
       
        if (!$media->isImage()){
            return null;
        }
        
        if (!$media->isRemoteUrl() && !$this->hasStorage($media)){
            return null;
        }
        
        $resize = $this->getResize($media, $width, $height);
        $useCloud = ($media->use_cloud && config('quatius.media.cloud_storage',false) && config('quatius.media.cloud_writable',false))?1:0;
        
        if(!$resize)
        {
            $resize = new MediaResize([
                'width'=>$width, 
                'height'=>$height, 
                'use_cloud'=>$useCloud
            ]);
            
            $resize->setRelation('media', $media);
            if (!$srcFile){
                $srcFile = $media->isRemoteUrl()?$media->source_url:$this->getStorage($media);
            }

            if ($allowStretch){
                $img = self::resizeImage($srcFile, $width, $height);
            }else {
                $img = self::resizeImageToCanvas($srcFile, $width, $height);
            }

            if ($this->putStorage($resize, $img->stream()->__toString()))
            {
                $media->resizes()->save($resize);
                $this->clearCache();
                return $resize;
            }
        }
        else if($recreate){
            $resize->setRelation('media', $media);
            
            $srcFile = $srcFile?:$this->getStorage($media);
            
            if ($allowStretch){
                $img = self::resizeImage($srcFile, $width, $height);
            }else {
                $img = self::resizeImageToCanvas($srcFile, $width, $height);
            }
            
            if ($this->putStorage($resize, $img->stream()->__toString()))
            {
                $resize->update(['use_cloud'=>$useCloud]);
                $this->clearCache();
                return $resize;
            }
        }else{
            $resize;
        }
        return null;
    }
    
    function deleteFiles($ids = array(), $deleteDB = true, $onlyResized=false){
        $medias = Media::whereIn('id',$ids)->get();
        $this->clearCache();
        $medias->each(function ($media) use($deleteDB, $onlyResized){
            $media->resizes->each(function($resize) use($deleteDB, $media){
                $resize->setRelation('media', $media);
                
                try{
                    $this->deleteStorage($resize);
                }catch(\Exception $e){
                    if (config("quatius.media.debug", false)) app('log').debug("Media- deleteFiles(".$e->getLine()."): ".$e->getMessage());
                }
                //TODO : $this->storage()->files(\File::dirname($resize->path));
                if ($deleteDB)
                    $resize->delete();
            });
            if (!$onlyResized){
            
                try{
                    $this->deleteStorage($media);
                }catch(\Exception $e){
                    if (config("quatius.media.debug", false)) app('log').debug("Media- deleteFiles(".$e->getLine()."): ".$e->getMessage());
                }

                if ($deleteDB)
                    $media->delete();
               
            }
        });
    }
}