<?php

namespace Quatius\Media\Repositories;

use Quatius\Media\Models\Media;

interface MediaRepository {
	
	function upload($file, $base_path='', $type = "");
	
	function resize($media, $width=0, $height=0, $recreate=false);
	
	function resizeToCanvas(Media $media, $width=0, $height=0, $recreate=false);
	
	function deleteFiles($ids = array(), $deleteDB = true, $onlyResized=false);
	
	function useCloud($setCloud = null);
	
	function getOptions($media, $onCloud = null);
	
	function putStorage($path, $onCloud = null, $file, $visible = 'public');
	
	function getStorage($path, $onCloud = null);
	
	function hasStorage($path, $onCloud = null);
	
	function url($src, $onCloud = null, $checkDB=true);
	
	function deleteStorage($path, $onCloud = null);
	
	function deleteLinkFrom($model, $id=null, $byMediaIds=null);
	
	function updateLink($media_ids, $model);
}