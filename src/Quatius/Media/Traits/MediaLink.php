<?php

namespace Quatius\Media\Traits;

/**
 * @author seyla.keth
 *
 */
trait MediaLink{
	
    public function getMediasAttribute(){
        return $this->medias();
    }
    
    public function medias(){
		if (!isset($this->getRelations()['medias'])){
			$this->setRelation('medias',app('MediaHandler')->getLinkFrom($this));
		}

        return $this->getRelation('medias');
    }
	
    public function hasMedia($group='image'){
        return !!$this->getMedias($group)->count();
	}
	
	public function filterTypes($types = ['image/png','image/jpeg','image/gif'], $group=''){
	    return $this->getMedias($group)->whereIn('mime_type',$types);
	}
	
	public function getImages($group='image'){
	    return $this->filterTypes(['image/png','image/jpeg','image/gif'], $group);
	}
	
	public function getThumbnail($group='image'){
	    return $this->getImages($group)->first();
	}
	
	public function getThumbnailUrl($label='thumbnail', $group='image'){
	    $mediaThumb = $this->getThumbnail($group);
		if ($mediaThumb)
		    return $mediaThumb->getThumbnailUrl($label);
		return "";
	}
	
	public function getMedia($group=''){
	    return $this->getMedias($group)->first();
	}
	
	public function getMediaUrl($group=''){
	    if ($this->getMedia($group)){
	        return $this->getMedia($group)->getMediaUrl();
	    }
	    return "";
	}
	
	public function getImageUrl($width=null, $height=null, $allow_stretch = true){
	    if ($this->getThumbnail()){
			return $this->getThumbnail()->getImageUrl($width, $height, $allow_stretch);
	    }
	    return "";
	}
	
	public function getMedias($group='image'){
	    if ($group){
	        return $this->medias->where('pivot.grouping',$group);
	    }
	    else{
	       return $this->medias;
	    }
	}
}