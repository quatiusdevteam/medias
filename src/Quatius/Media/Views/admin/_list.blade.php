<div class="col-xs-12">
	<div class="row">
	@foreach($medias as $media)
		@include('Media::admin._tile', compact('media'))
	@endforeach
	</div>
</div>
<div class="col-xs-12 text-right">
	@include('admin::curd.pagination', ['paginator' => $medias])
</div>