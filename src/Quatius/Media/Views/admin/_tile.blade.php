<div class="media-tile" style="text-align: center;width:120px;margin-left:5px;margin-bottom:5px; float:left" data-id="{!!$media->id!!}" >
	<div class="spacer" style="position: relative;">
		
		@if ($media->mime_type == 'image/jpeg' || $media->mime_type == 'image/png')
			<img style="display: inline" class="img-responsive"  src="{{media_url($media->getSize('thumbnail'))}}">
		@else
			<a href="{{media_url($media)}}" target="preview">click to view</a>
		@endif
		<div class="col-xs-12" style="height:40px; overflow:hidden">
			<div class="row">
			{!!$media->title!!}
			</div>
		</div>
		<button style="position: absolute; right: 0;top: 0;" class="btn-link remove-indicator" onclick="removeMedia(this)" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
	</div>
</div>
 