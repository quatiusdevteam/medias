@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> All Images
@stop
@section('title')
All images
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">All Files</li>
</ol>
@stop
@section('tools')
<script type="text/javascript">
var defaultPath="images";
</script>

	<button id="newImage" type="button" class="btn btn-primary btn-sm" onclick="getMedia(this, true, null, prevPath,'')" id="btn-new-user"><i class="fa fa-upload"></i> upload </button>
@show
@section('entry')
<div class="box box-warning" id='entry-product'>

</div>
@stop

@section('content')

<?php 
$mediaFolders = Quatius\Media\Models\Media::get()->groupBy('file_path');

$collapsed = true;
?>
<div id="manager-medias">
<div class="media-explorer" data-mime_type="" data-allow_duplicate="false" data-add-only="false">
<div class="col-xs-4">
	<ul id="manager-list-tree" class="path-list-tree" style="max-height:500px;">
		@include('Media::admin.partials.folder-list')
	</ul>
</div>
<div class="col-xs-8">
<form>
<div id="accordionFolder" class="media-listing" role="tablist" aria-multiselectable="true" class="selectMediaDialog" >
<?php
/*
foreach ($mediaFolders as $folder=>$medias)

    $folder_hashed = str_replace(['\'','"','\\','/','.'],'-',$folder);
    <div id="{{$folder_hashed}}" data-path="{{$folder}}" class="folder-files collapse {{$collapsed?'':'in'}}" role="tabpanel" aria-labelledby="heading-{{$folder}}">
   		include('Media::admin.partials.selection-list',["list_only"=>true,'grouping'=>"",'input_prefix'=>"",'onSelect'=>"",'add_only'=>true,'medias'=>$medias,'path'=>$folder, 'extras'=>'<button style="position: absolute; right: 0;top: 0;z-index: 40;" class="btn-link remove-indicator" type="button" data-toggle="popover" data-trigger="focus" = data-placement="left" data-html="true" title="Delete this media?" data-content="<div class=\'text-center\'><button type=\'button\' class=\'btn btn-warning btn-xs\' onclick=\'deleteMedia(this)\'>Yes</button> <button type=\'button\' class=\'btn btn-xs\'>No</button></div>"><i class="fa fa-times" aria-hidden="true"></i></button>'])
    </div>
endforeach
*/
?>
</div>
</form>
</div>
</div>
</div>
@include('Media::admin.partials.selection-script',['path_list'=>$mediaFolders->keys()])
@stop

@section('script')

@includeStyle('css/jquery-explr-1.4.css')
@includeScript('js/jquery-explr-1.4.js')

<script type="text/javascript">

function deleteMedia(itm)
{
	var jItm = jQuery(itm).parents('.media-tile');
	var id = jItm.attr('data-id');

	if (id == 0)
		return;

	var url = "{{url('/admin/medias')}}/"+id+"/delete";
	jItm.css('opacity','0.5');
	jQuery.get(url, function(response){
		jItm.remove();
	});
}

function selectedMedia(itm)
{
	var jItm = jQuery(itm).parents('.media-tile');
}

function searchMedia(itm)
{
	var search = jQuery('[name="search"][type="text"]').val();
	
	var url = "{{Request::url()}}?search="+encodeURIComponent(search);
	jQuery(itm).parents('.box').find('.box-body').empty();
	jQuery.get(url, function(response){
		jQuery(itm).parents('.box').find('.box-body').html(response);
	});
}

var inserted = 
$(document).ready(function(){
	loadTreeEvent($('#manager-medias .media-explorer'));
	//foldersTree = $("#folders-tree").explr();
        // $('.folder-item ul').removeClass('hide');

        // $('.folder-item a').on('click', function(e){
        //     $('.folder-files').collapse('hide');
        //     $(".folder-item a").removeClass('bg-success');
        //     $(this).addClass('bg-success');
        // });
	
	$('.media-tile [data-toggle="popover"]').popover();
	// $('#accordionFolder .folder-files').on('show.bs.collapse', function () {
	// 	defaultPath = $(this).data('path');
	// 	$('.folder-files').not(this).removeClass('active');
	// 	$(this).addClass('active');
	// });
	// $('#accordionFolder .folder-files').on('hide.bs.collapse', function () {
	// 	$('.folder-files').removeClass('active');
	// });
	// $('#selectMediaDialog').on('hidden.bs.modal', function () {
	// 	var checkPath = $('#selectMediaDialog .path-list input[name="path"]').val();
	// 	if ($('.box-tools .media-tile').length > 0){
	// 		window.location.reload();
	// 	}
	// });
	if ($('a[data-hasfile]').length > 0)
		prevPath=$('a[data-hasfile]:first').data('path');

	// $(document).on('media-uploaded',function( event, itm, newMedia, file, response){
	// 	var inserted = $(response.view);
	// 	var path = $('#selectMediaDialog .path-list input[name="path"]').val();

	// 	inserted.find('[data-toggle="popover"]').popover();
	// 	$('#accordionFolder .folder-files[data-path="'+path+'"] .media-container').append(inserted);
	// });
}); 
</script>
@stop

@section('style')
@stop
