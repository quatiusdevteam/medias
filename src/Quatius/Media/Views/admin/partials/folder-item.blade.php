@php
$parentFolder = str_replace("&#46;",".", $parentFolder);
$path = str_replace("&#46;",".", $path);
$path_hashed = str_replace(['\'','"','\\','/',' ',"."],'-',$path);
@endphp
<li class="folder-item {{ $subFolder?'icon-folder':'icon-photo'}}"> <a data-path="{{$path}}" role="button" data-toggle="collapse" data-hasfile="{{$folderCollect->contains($path)?'true':'false'}}" href="#{{$folderCollect->contains($path)?$path_hashed:''}}" aria-expanded="true" aria-controls="{{$path_hashed}}">{{$parentFolder}}</a>
    @if($subFolder)
    <ul class="hide">
    @foreach ($subFolder as $folderName=>$folderData)
        <?php if (!$folderName) continue;?>
        @include('Media::admin.partials.folder-item',["parentFolder"=>$folderName, "subFolder"=>$folderData,"path"=>$path."/".$folderName])
    @endforeach
    </ul>
    @endif
</li>