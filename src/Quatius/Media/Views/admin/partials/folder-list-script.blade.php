
@includeStyle('css/jquery-explr-1.4.css')
@includeScript('js/jquery-explr-1.4.js')

@script
<script>
    var foldersTree = null;
    $(document).ready(function() {
        foldersTree = $("#folders-tree").explr();
        $('.folder-item ul').removeClass('hide');

        $('.folder-item a').on('click', function(e){
            $('.folder-files').collapse('hide');
            $(".folder-item a").removeClass('bg-success');
            $(this).addClass('bg-success');
        });

        //open
        //$('[data-path="nz/pages"]').parents('.folder-item').find('.explr-plus').click();

        //close all
        //$('.folder-item .explr-minus').click();
    });
</script>
@endscript

<ul id="folders-tree" style="max-height:500px;">
    @include('Media::admin.partials.folder-list')
</ul>