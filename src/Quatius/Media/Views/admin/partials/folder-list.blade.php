
@php
    $mediaFolders = isset($mediaFolders)?$mediaFolders: \Quatius\Media\Models\Media::get()->groupBy('file_path');
    $folderCollect = $mediaFolders->keys();
    $folderGrp = array_tree($folderCollect->toArray());
@endphp

@foreach ($folderGrp as $folderName=>$folderData)
    <?php if (!$folderName) continue;?>
    
    @include('Media::admin.partials.folder-item',["parentFolder"=>$folderName, "subFolder"=>$folderData,"path"=>$folderName])
@endforeach
