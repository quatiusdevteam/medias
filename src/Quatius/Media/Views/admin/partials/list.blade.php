<div class="row">
<div class="col-xs-12">
	<div class="row media-selection-list">
	@foreach($medias as $media)
		@include('Media::admin.partials.tile', ['media'=>$media])
	@endforeach
	</div>
</div>
</div>
