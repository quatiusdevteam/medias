<?php 
    $grouping = isset($grouping)?$grouping:'image';
    $path = isset($path)?$path:'';
    $mime_types = isset($mime_types)?$mime_types:'';//'image/png,image/jpeg'
    $medias= isset($medias)?collect($medias):collect([]);
    $extras = isset($extras)?$extras:'<a href="#" style="position: absolute; left: 10px;top: 0;z-index: 40; cursor: move;" draggable="true" class="dd-handle-move"><i class="fa fa-arrows-alt"></i></a><button style="position: absolute; right: 0;top: 0;" class="btn-link remove-indicator" onclick="$(this).parents(\'.media-tile\').remove()" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>';
    $input_prefix = isset($input_prefix)?$input_prefix:"medias"; 
    $onSelect = isset($onSelect)?$onSelect:'';
    $add_only = isset($add_only)?$add_only:false;
    $list_only = isset($list_only)?$list_only:false;
    $allow_duplicate = isset($allow_duplicate)?$allow_duplicate:false;
    $media_index = 0;

    $attributes = isset($attributes)?$attributes:[];//['data-multi-select'=>'true', 'data-inline'=>'true'];
?>

<div class="media-selection-area" data-allow_duplicate="{{$allow_duplicate?'true':'false'}}" data-size="{{$medias->count()}}" data-input_prefix='{!!$input_prefix!!}' data-mime_type='{!!$mime_types!!}' data-path='{{$path}}' data-grouping='{{$grouping}}' data-on_select='{{$onSelect}}'
    @foreach ($attributes as $data_name=>$data_value)
        {{' '.$data_name}}="{{$data_value}}"
    @endforeach
    >
<div class="col-xs-12">
	<div class="row link-media-list">

        <div class="dd" id="nestable" >
            <div class="media-container dd-list">
                @php
                    $listMedias = $medias->where('pivot.grouping', $grouping);
                    if ($listMedias->count()==0 && $grouping == "")
                        $listMedias = $medias;
                @endphp
                
                @foreach($listMedias as $media)
                    <?php
                    $media_index = isset($media->pivot->ordering)?$media->pivot->ordering:0;
                    $item_extras = $extras;
                    if ($input_prefix != ""){
                        $item_extras.='<input type="hidden" class="media_id" name="'.$input_prefix.'['.$media_index.'][media_id]" value="'.$media->id.'"/>';
                        $item_extras.='<input type="hidden" class="ordering" name="'.$input_prefix.'['.$media_index.'][ordering]" value="'.(isset($media->pivot->ordering)?$media->pivot->ordering:"").'"/>';
                        $item_extras.='<input type="hidden" class="grouping" name="'.$input_prefix.'['.$media_index.'][grouping]" value="'.$media->pivot->grouping.'"/>';
                        $item_extras.='<input type="hidden" class="params" name="'.$input_prefix.'['.$media_index.'][params]" value="'.(isset($media->pivot->params)?htmlentities($media->pivot->params):"").'"/>';
                    }
                    ?>
                    @include('Media::admin.partials.tile', ['media'=>$media,"extras"=>$item_extras,"onSelect"=>$onSelect])

                @endforeach
            </div>
        </div>
            @if(!$list_only)
        <div class="add-button img-thumbnail pull-left" style="cursor: pointer;text-align: center;width:120px;margin:2px;height: 135px;" onclick="getMedia(this,{{$add_only?'true':'false'}})">
            <i style="    font-size: 70px;
margin-top: 20px;
color: #dcdcdc;" class="fa fa-plus"></i>
        </div>
    @endif
    </div>
</div>
<div class="media-extras" style="display:none">{!!$extras!!}</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    
	$('.dd').nestable().on('change', function(e){
        var out = $(e.target).nestable('serialize');
        out     = JSON.stringify(out);
        var formData = new FormData();
        formData.append('tree', out);
	});
});
</script>
