<?php 
    $selectionMediaScriptLoaded = isset($selectionMediaScriptLoaded);
?>

@if (!$selectionMediaScriptLoaded)

@includeStyle('css/jquery-explr-1.4.css')
@includeScript('js/jquery-explr-1.4.js')

<?php 

    $selectionMediaScriptLoaded = true;
    $action_url = url('admin/medias/upload');
    $li_paths = view('Media::admin.partials.folder-list')->render(); 

$srcDialog = <<< EOF

<div class="modal fade selectMediaDialog" id="selectMediaDialog" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Select an image</h4>
			</div>
			<div class="modal-body">
                <div class="row media-explorer" data-mime_type="" data-allow_duplicate="false" data-add-only="false">
					<div class="col-md-4 path-list">
						<div>
							<ul id="dialog-list-tree" class="path-list-tree" style="max-height:200px;">
							$li_paths
							</ul>
						</div>
						<div class="clearfix"></div>
        				<form style="padding:0px" id="media-dropzone" method="post" action="$action_url" class="dropzone">
							<div class="input-group" style=" width: 100%;">
								<span class="input-group-addon">Path: </span>
                              <input type="text"  name="path" value="images" class="form-control" aria-label="...">
                            </div>
        				</form>
                    </div>
    				<div class="media-listing col-md-8" style="overflow-y: auto;max-height: 400px;"></div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
		</div>
	</div>
</div>

EOF;
?>

<style>
.selectMediaDialog .dz-preview{
    margin: 6px;
}
.media-selection-area.selecting .link-media-list{
    position: relative;
    z-index: 1051;
    background: #aaa;
	min-height: 139px;
}
.media-selection-area.selecting .add-button{
	display: none;
}

</style>

@includeScript('js/vendor_quatius_media_drag_drop.js')

@script
<script type="text/javascript">
var srcDialog = '{!! str_replace(["\n","\r","\t"],"",$srcDialog) !!}';
var completedUpload = typeof completedUpload == "undefined"?null:completedUpload;
var completedSelect = typeof completedSelect == "undefined"?null:completedSelect;
var mediaSelect = null;

var mediaDropzone;
var g_options = {};

var addedbutton = null;
var parentArea = null;

var prevPath = 'images';

function setPath(itm)
{
	var jItm = $(itm);
	var path = jItm.data('path');
	var explorer = jItm.parents('.media-explorer');

	prevPath = path;

	if (explorer.find('input[name="path"]').length)
		explorer.find('input[name="path"]').val(path);
	
	if (explorer.attr("data-add-only") == "true"){
		return;
	}

	explorer.find('.media-listing .folder-content').hide();
	if (explorer.find('.folder-content[data-path="'+path+'"][data-mime_type="'+explorer.attr("data-mime_type")+'"]').length > 0)
	{
		explorer.find('.media-listing .folder-content[data-path="'+path+'"]').show();
		return;
	}
	
	loadPath(path, explorer);
}

function loadTreeEvent(explorer){
	explorer.find('.folder-item a').attr('href', '#');
	explorer.find('.path-list-tree').explr();
	explorer.find('.folder-item ul').removeClass('hide');

	explorer.find('.folder-item a').on('click', function(e){
		setPath(this);
		explorer.find('.folder-item a').removeClass('bg-success');
		$(this).addClass('bg-success');
	});
}

function getMedia(itm, setAddOnly, selectCallback, filePath, type){
	addedbutton = $(itm);
	
	if (typeof Dropzone !== 'function') return; // check if page completely loaded

	if (selectCallback){
		completedSelect = selectCallback;
	}
	
	parentArea = addedbutton.parents('.media-selection-area');
	
	if (parentArea.length == 0){
		filePath = filePath?filePath:'images';
		
		type = type!==undefined?type:'image/png,image/jpeg';
		parentArea = $('<div class="media-selection-area" data-allow_duplicate="false" data-size="1" data-input_prefix="" data-mime_type="'+type+'" data-path="'+filePath+'" data-on_select="">');
	}else{
		if (filePath)
			parentArea.data('path', filePath);
		else{
			filePath = parentArea.attr("data-path");
			if (parentArea.find(".media-tile:last-child").length)
				filePath = parentArea.find(".media-tile:last-child").attr('data-path');
		}
	}

	var options={
		mime_types : parentArea.attr("data-mime_type"),
		input_prefix : parentArea.attr("data-input_prefix"),
	};
	
	g_options = options;
	try{
		if ($('#selectMediaDialog').length == 0){
			$(srcDialog).appendTo('body');
			
			loadTreeEvent($('#selectMediaDialog .media-explorer'));
			
			$('#selectMediaDialog').on('shown.bs.modal', function () {
				if (addedbutton == null || parentArea == null) return;

				$('#selectMediaDialog .folder-item a[data-path="'+filePath+'"]').focus();
				let mediaListHeight = parentArea.find(".link-media-list").outerHeight(true);
				if(parentArea.attr("data-inline") == "true"){
					parentArea.addClass("selecting");
					let scrollPos = parentArea.offset().top;
					let combineHeight = mediaListHeight + $('#selectMediaDialog .modal-dialog').outerHeight();
					if (combineHeight > $(window).height()){
						scrollPos += (combineHeight - $(window).height());
						mediaListHeight -= combineHeight - $(window).height();
					}
					$('#selectMediaDialog .modal-dialog').css('margin-top', mediaListHeight);
					$([document.documentElement, document.body]).animate({ scrollTop: scrollPos}, 1000);
					
				}
			}).on('hide.bs.modal', function () {
				$(".media-selection-area").removeClass("selecting");
			})
		}else{
			mediaDropzone.destroy();
			$('#selectMediaDialog .folder-item a').removeClass('bg-success');
		}
	
		applyDropzone('#media-dropzone', options);
	}catch(e){
		$('#selectMediaDialog').remove();
		return;
	}
	$('#media-dropzone .dz-preview').remove();
	
	//close all
	$('#selectMediaDialog .folder-item .explr-minus').click();
	//open
	$('#selectMediaDialog .folder-item a[data-path="'+filePath+'"]').parents('.folder-item').find('>.explr-plus').click();
	//hilight
	$('#selectMediaDialog .folder-item a[data-path="'+filePath+'"]').addClass('bg-success');
	
	$('#selectMediaDialog .modal-title').html("Select a "+(parentArea.attr("data-mime_type")==""?"file":parentArea.attr("data-mime_type")));

	$('#selectMediaDialog').find('.media-explorer')
		.attr("data-mime_type",parentArea.attr("data-mime_type"))
		.attr("data-allow_duplicate",parentArea.attr("data-allow_duplicate"))
		.attr("data-add-only",setAddOnly);

	if (setAddOnly)
	{
		$('#selectMediaDialog form input[name="path"]').val(filePath);
		$('#selectMediaDialog form').parent().removeClass('col-md-4').addClass('col-md-12');
		$('#selectMediaDialog').modal('show');
		return;
	}else if ($('#selectMediaDialog form').parent().hasClass('col-md-12')){
		$('#selectMediaDialog form').parent().removeClass('col-md-12').addClass('col-md-4');
	}
	
	if ($('#selectMediaDialog .folder-content[data-path="'+filePath+'"][data-mime_type="'+parentArea.attr("data-mime_type")+'"]').length > 0)
	{
		$('#selectMediaDialog .folder-content').hide();
		
		$('#selectMediaDialog .folder-content[data-path="'+filePath+'"][data-mime_type="'+parentArea.attr("data-mime_type")+'"]').show();
		$('#selectMediaDialog form input[name="path"]').val(filePath);
		
		$('#selectMediaDialog .media-tile').css('opacity',"1");
		if (parentArea.attr('data-allow_duplicate')=='false')
		{
			$('#selectMediaDialog .media-tile').each(function(index){
				if (parentArea.find('input.media_id[value="'+$(this).attr('data-id')+'"]').length > 0)
				{
					$(this).css('opacity',"0.5");
				}
			});
		}
		$('#selectMediaDialog').find('.path-list li[data-path="'+filePath+'"] a').click();

		if (parentArea.attr("data-inline") == "true"){
			let mediaListHeight = parentArea.find(".link-media-list").outerHeight(true);
			$('#selectMediaDialog .modal-dialog').css('margin-top', mediaListHeight);
			let scrollPos = parentArea.offset().top;
			let combineHeight = mediaListHeight + $('#selectMediaDialog .modal-dialog').outerHeight();
			if (combineHeight > $(window).height())
				scrollPos -= (combineHeight - $(window).height());
				
			$([document.documentElement, document.body]).animate({ scrollTop: scrollPos}, 1000);
		}
		$('#selectMediaDialog').modal('show');
		return;
	}

	$('#selectMediaDialog form input[name="path"]').val(filePath);
	loadPath(filePath, $('#selectMediaDialog').find('.media-explorer'));
}

function loadPath(path, explorer){
	
	var mime_types = explorer.attr("data-mime_type");
	var allow_duplicate = explorer.attr("data-allow_duplicate");

	var loadedPath = path; 
	
	
	if (explorer.find('.folder-content[data-path="'+loadedPath+'"][data-mime_type="'+mime_types+'"]').length > 0){
		
	}
	else { // loading new contents
		explorer.find('.media-listing')
			.append($('<div class="folder-content" data-path="'+loadedPath+'" data-mime_type="'+mime_types+'">').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> Loading...'));
	}

	$.getJSON(baseUrl+"admin/medias/list?path=" + loadedPath + "&mime_types=" + mime_types, function(jsonData){
		
		if (explorer.find('.folder-content[data-path="'+loadedPath+'"][data-mime_type="'+mime_types+'"]').length > 0){
			explorer.find('.folder-content[data-path="'+loadedPath+'"][data-mime_type="'+mime_types+'"]').html(jsonData.view).hide();
		}
		else{
			$newlisting = $('<div class="folder-content" data-path="'+loadedPath+'" data-mime_type="'+mime_types+'">').html(jsonData.view);
			explorer.find('.media-listing').append($newlisting);
		}

		explorer.find('.folder-content[data-path="'+loadedPath+'"][data-mime_type="'+mime_types+'"] .media-tile [data-toggle="popover"]').popover();
		
		if(explorer.find('form input[name="path"]').length){
			var dispPath = explorer.find('form input[name="path"]').val();
			
			explorer.find('.folder-content').not('[data-path="'+dispPath+'"][data-mime_type="'+mime_types+'"]').hide();
			
			explorer.find('.folder-content[data-path="'+dispPath+'"][data-mime_type="'+mime_types+'"]').show();
			
			explorer.find('.media-tile').css('opacity',"1");
			if (allow_duplicate!=null &&  allow_duplicate=='false')
			{
				explorer.find('.media-tile').each(function(index){
					if (parentArea && parentArea.find('input.media_id[value="'+$(this).attr('data-id')+'"]').length > 0)
					{
						$(this).css('opacity',"0.5");
					}
				});
			}
		}else{
			explorer.find('.folder-content').not('[data-path="'+loadedPath+'"][data-mime_type="'+mime_types+'"]').hide();
			
			explorer.find('.folder-content[data-path="'+loadedPath+'"][data-mime_type="'+mime_types+'"]').show();
		}

		if (explorer.parents('.modal').length){
			explorer.parents('.modal').modal('show');
		}
	});
}

var mediaDropzone = null;

function applyDropzone(dropId, options){

	if (mediaDropzone != null)
		mediaDropzone.destroy();
	
	if (options.mime_types == "")
	{
		mediaDropzone = new Dropzone(dropId, { 
			url: baseUrl+"admin/medias/upload",
			headers: {
				'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
				},
			params:options
		});
				
	}else{
		mediaDropzone = new Dropzone(dropId, { 
			url: baseUrl+"admin/medias/upload",
			headers: {
				  'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
				},
			acceptedFiles: options.mime_types,
			params:options
		});
	}
	
	mediaDropzone.on('success', function( file, resp ){
		if (completedUpload != null){
			completedUpload(file, resp);
		}
		var newMedia = null;
		if ($('#selectMediaDialog .media-selection-list .media-tile[data-id="'+resp.id+'"]').length == 0){
			var path = $('#selectMediaDialog input[name="path"').val();
			if($('#selectMediaDialog .path-list').find('[data-path="'+path+'"]').length == 0)
			{
				if (resp.path_view != ""){
					$('.path-list-tree').html(resp.path_view);
					loadTreeEvent($('#selectMediaDialog'));
				}
				
				if ($('#selectMediaDialog .media-explorer').attr("data-add-only") == "false"){
					loadPath(path, $('#selectMediaDialog .media-explorer'));
				}
				
				return
			}	
				
			newMedia = $(resp.view);
			newMedia.find('[data-toggle="popover"]').popover();
			$('#selectMediaDialog .folder-content:visible .media-selection-list').append(newMedia);
			
		}
		else{
			newMedia = $('#selectMediaDialog .media-selection-list .media-tile[data-id="'+resp.id+'"] img');
			newMedia.attr('src', newMedia.attr('src')+"?"+ new Date());
		}

		$(document).trigger('media-uploaded', [addedbutton, newMedia, file, resp ]);
	});
}

function selectedMedia(itm){
	
	mediaSelect = $(itm).parents('.media-tile');
	var media_id = mediaSelect.attr('data-id');
	
	if (parentArea.attr('data-allow_duplicate')=='false')
	{
		if (parentArea.find('input.media_id[value="'+media_id+'"]').length > 0)
			return;
	}
	var mediaContainter = parentArea.find('.media-container');
	
	var newThumb = mediaSelect.clone();
	if (mediaContainter.length > 0){
    	mediaContainter.append(newThumb);
        applyDragDrop(newThumb);
    	newThumb.find('.media-click').attr('onclick', parentArea.attr('data-on_select'));
    	newThumb.find('.remove-indicator').remove();
    	if (parentArea.find('.media-extras').length > 0){
    		newThumb.find('.spacer').append(parentArea.find('.media-extras').children().clone());
    	}
    	if (g_options.input_prefix != ""){
    		var media_index = parseInt(parentArea.attr('data-size'));
    		media_index = media_index?media_index:0;

			if (media_index == 0){
				media_index = $('.media-selection-area .media-tile').length;
			}
			
			media_index++;

			$('.media-selection-area').attr('data-size', media_index);
			
    		newThumb.find('.spacer').append('<input type="hidden" class="media_id" name="'+g_options.input_prefix+'['+media_index+'][media_id]" value="'+media_id+'"/>');
    		newThumb.find('.spacer').append('<input type="hidden" class="ordering" name="'+g_options.input_prefix+'['+media_index+'][ordering]" value=""/>');
    		newThumb.find('.spacer').append('<input type="hidden" class="grouping" name="'+g_options.input_prefix+'['+media_index+'][grouping]" value="'+parentArea.data('grouping')+'"/>');
    		newThumb.find('.spacer').append('<input type="hidden" class="params" name="'+g_options.input_prefix+'['+media_index+'][params]" value=""/>');
    		
    		
    			
    	}
	}
	applySelection(newThumb, mediaSelect);
}

function applySelection(newThumb, mediaSelect){
	var isClose = true;
	if (completedSelect!=null)
	{
		completedSelect(newThumb, addedbutton, mediaSelect);
	}

	isClose = parentArea.attr('data-multi-select') != "true";

	$(document).trigger('media-selected', [addedbutton, newThumb, mediaSelect]);

	if (isClose == null)
		isClose = true;

	if (isClose)
		$('#selectMediaDialog').modal('hide');
}

function deleteMedia(itm)
{
	if ($(itm).parents('.selectMediaDialog').length==0)
		return;
	
	var jItm = jQuery(itm).parents('.media-tile');
	var id = jItm.attr('data-id');
	
	if (id == 0)
		return;

	var url = baseUrl+"admin/medias/"+id+"/delete";
	jItm.css('opacity','0.5');
	jQuery.get(url, function(response){
		$('.media-tile[data-id="'+id+'"]').remove();
		
		$(document).trigger('media-deleted', [addedbutton, id ]);
	});
}

function applyDragDrop(itm){
	var move_icon = '<a href="#" style="position: absolute; left: 10px;top: 0;z-index: 40; cursor: move;" class="dd-handle-move"><i class="fa fa-arrows-alt"></i></a>';
	itm.append(move_icon);
    itm.parents('div.media-container').addClass('dd-list');
    itm.addClass('dd-item');
    $('.dd').nestable().on('change', function(e){
        var out = $(e.target).nestable('serialize');
        out     = JSON.stringify(out);
        var formData = new FormData();
        formData.append('tree', out);
	});
}

//Page content 

function setPageMedia (media){
	savedSelectMedia = $(media);
	
	if (focusedInputUrl){
		focusedInputUrl.val($(media).data('media_url'));
		focusedInputUrl.trigger('keyup');
		focusedInputUrl.trigger('change');
		$('.note-image-btn').prop('disabled',false);
	}
}

function summernoteMediaPlugin(itm)
{
    $(document).ready(function(){
    	app.sendFile =  function(file, url, editor) {
        	console.log('Media upload has not been implemented.');
			/* 
        	//TODO: create link for media upload
			var data = new FormData();
		    data.append("file", file);
		    $.ajax({
		        data: data,
		        type: "POST",
		        url: url,
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function(objFile) {
		            editor.summernote('insertImage', objFile.folder+objFile.file);
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		        }
		    }); */
		}
    	updateSummernoteEvents(itm);
    });
    
    $( document ).ajaxComplete(function() {
    	updateSummernoteEvents(itm);
    });
}

var savedSelectMedia = null;
var focusedInputUrl = null;
var pagePath = "pages";

function updateSummernoteEvents(itm){
	$(itm).on('summernote.init', function() {
		pagePath = $(this).data('path')?$(this).data('path'):"pages";
		
		if ($(this).prop('disabled')){
			if ($(this).next().hasClass('note-editor'))
			{
				$(this).next().find('.note-toolbar.btn-toolbar').remove();
			}
			return;
		}
		var videoInput = $(this).next().find('.note-video-attributes-href');
    	if (videoInput.length > 0){
			
			// var modelVidBody = $(this).parents('.modal-body');
			// if (modelVidBody.find('.select-vid-attr').length == 0){
			// 	$(this).parents('.modal-body').find('.note-video-attributes-href.form-control').after('<span class="input-group-btn"><button class="btn btn-default select-vid-attr" type="button">Go!</button></span>');
			// 	$(this).parents('.modal-body').find('.note-video-attributes-href.form-control').after('<span class="input-group-btn"><button class="btn btn-default select-vid-attr" type="button">Go!</button></span>');
			// }

			videoInput.on('focus',function(){
				var modelVidBody = $(this).parents('.modal-body');
				
        		focusedInputUrl = $(this);
        		if (modelVidBody.length > 0){
            		if (modelVidBody.find('.select-vid-attr').length == 0){
                		modelVidBody.find('.note-video-attributes-href').after('<span class="input-group-btn"><button class="btn btn-primary select-vid-attr" type="button"onclick="getMedia(this,false, function(media){setPageMedia(media);}, $(this).parents(\'.note-editor\').prev().data(\'path\'),\'video/mp4\')">Select Video</button></span>');
                	}
            		else if(modelVidBody.find('.media-selection-area') > 0){
            			modelVidBody.find('.media-selection-area').attr('data-path',pagePath);
					}
        		}
           	}).on('change',function(){
           		
           		var formContainer = $(this).parents('form');
           		if(savedSelectMedia.data('media_url') == $(this).val() && $('input.media_id[value="'+savedSelectMedia.data('id')+'"]').length == 0){
           			formContainer.append('<input type="hidden" class="media_id" data-media_url="'+savedSelectMedia.data('media_url')+'" name="page_medias[][media_id]" value="'+savedSelectMedia.data('id')+'" disabled>');
           		}
            });
		}
    	var imageInput = $(this).next().find('.note-image-url');
    	if (imageInput.length > 0){
        	
    		imageInput.on('focus',function(){
        		var modelBody = $(this).parents('.modal-body');
        		focusedInputUrl = $(this);
        		if (modelBody.length > 0){
            		if (modelBody.find('.btn-saved-media').length == 0){
                		modelBody.prepend('<button type="button" class="btn btn-info btn-saved-media" onclick="getMedia(this,false, function(media){setPageMedia(media);}, $(this).parents(\'.note-editor\').prev().data(\'path\'),\'image/png,image/jpeg,image/gif\')"><i class="fa fa-image"><i/> Select Images </button>');

                		modelBody.find('.note-group-select-from-files').remove();
                	}
            		else if(modelBody.find('.media-selection-area') > 0){
            			modelBody.find('.media-selection-area').attr('data-path',pagePath);
					}
        		}
           	}).on('change',function(){
           		
           		var formContainer = $(this).parents('form');
           		if(savedSelectMedia.data('media_url') == $(this).val() && $('input.media_id[value="'+savedSelectMedia.data('id')+'"]').length == 0){
           			formContainer.append('<input type="hidden" class="media_id" data-media_url="'+savedSelectMedia.data('media_url')+'" name="page_medias[][media_id]" value="'+savedSelectMedia.data('id')+'" disabled>');
           		}
            });
    	}
	}).on('summernote.change', function(we, contents, editable) {
		var srcs= new Array();
		editable.find('img').each(function(index){
			srcs.push($(this).attr('src'));
		});

		var formContainer = $(this).parents('form');

		// check whether we use the media in the contents.
		formContainer.find('input.media_id[data-media_url]').each(function(){
			if ($.inArray($(this).data('media_url'), srcs) == -1){
				$(this).prop('disabled', true);
			}
			else{
				$(this).prop('disabled', false);
			}
		});
	});
}

function setMediaTrigger(){
	$('.get-media').not('.media-init').addClass('media-init').on(
		{
			click: function() {
				var jItm = $(this);
				var mime = jItm.data('mime')?jItm.data('mime'):"image/png,image/jpeg,image/gif";
				var path = jItm.data('path')?jItm.data('path'):"images";

				var target = jItm.data('target')?$(jItm.data('target')):jItm;
				
				jItm.trigger('media-selecting');

				getMedia(jItm,false, 
					function(media){
						if (target.is( "input" )){
							target.val($(media).data('media_url'));
							target.trigger('keyup');
							target.trigger('change');
							target.focus();
						}else{
							target.data('media_src', $(media).data('media_url'));
							target.attr('data-media_src', $(media).data('media_url'));
						}
						jItm.trigger('media', [media, target]);
					},
					path,
					mime
				);
			}
		}
		);
		
}

$(document).ready(function(){
    $('.dd').nestable().on('change', function(e){
        var out = $(e.target).nestable('serialize');
        out     = JSON.stringify(out);
        var formData = new FormData();
        formData.append('tree', out);
	});
	setMediaTrigger();
});
</script>
@endscript

@endif
