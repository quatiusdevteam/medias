
<?php $extras = isset($extras)?$extras:'<button style="position: absolute; right: 0;top: 0;z-index: 40;" class="btn-link remove-indicator" type="button" data-toggle="popover" data-trigger="focus" = data-placement="left" data-html="true" title="Delete this media?" data-content="<div class=\'text-center\'><button type=\'button\' class=\'btn btn-warning btn-xs\' onclick=\'deleteMedia(this)\'>Yes</button> <button type=\'button\' class=\'btn btn-xs\'>No</button></div>"><i class="fa fa-times" aria-hidden="true"></i></button>';?>
<?php $onSelect = isset($onSelect)?$onSelect:'selectedMedia(this)'; ?>
<?php $input_prefix = isset($input_prefix)?$input_prefix:""; ?>

<div class="media-tile img-thumbnail pull-left dd-item"  style="text-align: center;width:120px;margin:2px;height: 135px;" data-id="{!!$media->id!!}" data-path="{!!$media->file_path!!}" data-mime_type="{{$media->mime_type}}" data-media_url="{{media_url($media)}}">
	<div class="spacer" style="position: relative;">
		
		@if ($media->isImage())
			<img class="media-click" onclick="{!!$onSelect!!}" style="{!!$onSelect==''?'':'cursor:pointer';!!} width:110px; height: 83px; display: inline" src="{{media_url($media->getSize('thumbnail'))}}">
		@else
			<div class="media-click" onclick="{!!$onSelect!!}" style="{!!$onSelect==''?'':'cursor:pointer';!!} width:110px; height: 83px; display: block">
				<i class="fa fa-file-code-o" style="font-size: 40px; margin-top: 13px;"></i>
				<h6 style="height:30px; overflow:hidden;">{{$media->mime_type}}</h6>
			</div>
		@endif
		@if($media->isRemoteUrl())
		<i style="position: absolute; right: 10px; top: 70px;" class="fa fa-rss-square" aria-hidden="true" title="Remote URL: {{$media->source_url}}"></i>
		@endif
		<h5 style="height:30px; overflow:hidden;">{!!$media->title!!}</h5>
			{!!$extras!!}
	
	</div>

</div>