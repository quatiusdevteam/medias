@extends('layouts.dashboard')

@section('header-link')
<link rel="stylesheet" href="{{url('css/dropzone.min.css')}}" />
@endsection

@section('content')
{!! Form::open(['name'=>'file_upload_form',
	'id'=>'file_upload_form',
	'method'=>'POST',
	'url'=>'admin/medias/upload','class'=>'dropzone']) 
!!}
<div style="position: absolute; top: 10px;">{!! Form::label('media_type','Type: ') !!}{!! Form::radio('media_type', 0 ,true,["id"=>"product_type"]) !!}{!! Form::label('product_type','Product ') !!} {!! Form::radio('media_type', 1 ,false,["id"=>"other_type"]) !!}{!! Form::label('other_type','Other images') !!}</div>

{!! Form::close() !!}
@endsection

@section('footer-javascript')
<script src="{{url('js/dropzone.min.js')}}"></script>
@endsection