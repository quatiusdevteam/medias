<?php
return[
    'debug' => env('MEDIA_DEBUG', false),
	'cloud_storage' => env('MEDIA_USE_CLOUD', false),
    'cloud_writable' => env('MEDIA_CLOUD_WRITABLE', !env('APP_DEBUG', true)),
    'fetch_remote_url' => env('MEDIA_FETCH_REMOTE_URL', ''),
    'resize_name_prefix' => "sizes/",
    'memory_limit' => env('MEDIA_MEMORY_LIMIT', ""),
    'root_path' => env('MEDIA_ROOT', 'medias/'),
    
    'default_path' => "images",

    'check_url_exist' => env('MEDIA_URL_CHECKING', false),
    'not_found_url' => env('MEDIA_NOT_FOUND_URL', ''),
    'allow_stretch' => true,
    
    'sizes' => [
        "thumbnail"=>["width"=>120, "height"=>90, "allow_stretch"=>false, "create_on_upload"=>true],
        "facebook_lg"=>["width"=>1200, "height"=>630, "allow_stretch"=>false],
        "facebook_xs"=>["width"=>600, "height"=>315, "allow_stretch"=>false]
    ],
    'image_supports' => ['jpg','jpeg','png','gif']
];