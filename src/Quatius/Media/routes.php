<?php

Route::group(['prefix'=>'admin', 'module' => 'Media',  'namespace' => 'Quatius\Media\Controllers'],function(){
	Route::group(['prefix'=>'medias'],function(){
		Route::get('/{id}/delete', 'MediaController@delete');
		//Route::get('/product', 'MediaController@indexProduct');
		Route::post('/upload/{path?}', 'MediaController@uploadMedia');
		Route::get('/', 'MediaController@index');
		Route::get('test', 'MediaController@test');
		Route::get('list/{path?}', 'MediaController@getMediaPath');
		/* Route::post('{product_id}/medias', 'Admin\ProductsController@addMedias')->where('product_id','[0-9]+');
		Route::delete('{product_id}/medias','Admin\ProductsController@detachMedias')->where('product_id','[0-9]+'); */
	});
});